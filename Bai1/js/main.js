const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

const renderButton =() => {
    let contentHMTL = ""; 
    for ( let i = 0; i < colorList.length; i++) {
        const color = colorList[i] 
        contentHMTL += 
            `<button class="btn color-button ${color}" onclick="changeColor('${color}')"></button>`
    }
    document.getElementById("colorContainer").innerHTML = contentHMTL; 
}

renderButton(); 

const changeColor = (color) => {

   document.getElementById("house").className = `house ${color}`
}