averageScore = (...rest) => {
    let score = 0;
    for (let i = 0; i < rest.length; i++) {
        score += rest[i];
    }
    return score/rest.length
}

averageScore1 = () => {
    let math = document.getElementById("inpToan").value*1; 
    let physic = document.getElementById("inpLy").value*1;
    let chemistry = document.getElementById("inpHoa").value*1;
    let result1 = (averageScore(math, physic, chemistry)).toFixed(2)
    document.getElementById("tbKhoi1").innerHTML = result1
}

averageScore2 = () => {
    let literature = document.getElementById("inpVan").value*1; 
    let history = document.getElementById("inpSu").value*1;
    let geogaphy = document.getElementById("inpDia").value*1;
    let english = document.getElementById("inpEnglish").value*1;
    let result2 = (averageScore(literature, history, geogaphy, english)).toFixed(2)
    document.getElementById("tbKhoi2").innerHTML = result2
}


